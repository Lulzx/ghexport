Script for backing up your Github events stream.

Prerequisites:
    
    `pip3 install -r requirements.txt`

Usage:

    cp config.py.example config.py

Afterwards, edit `config.py`: set URL and backup directory.
